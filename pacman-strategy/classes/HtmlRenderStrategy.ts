import {IStrategy} from "../interfaces/IStrategy.ts";

export class HtmlRenderStrategy implements IStrategy {
    render() {
        console.log("Rendering Pacman in HTML mode with image <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Pacman.svg/440px-Pacman.svg.png\" />");
    }
}