import {IStrategy} from "../interfaces/IStrategy.ts";

export class TextRenderStrategy implements IStrategy {
    render() {
        console.log("Rendering Pacman in text mode with character '@'");
    }
}