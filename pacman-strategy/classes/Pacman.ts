import {IStrategy} from "../interfaces/IStrategy.ts";

export class Pacman {
    private renderStrategy: IStrategy;

    constructor(renderStrategy: IStrategy) {
        this.renderStrategy = renderStrategy;
    }

    render() {
        this.renderStrategy.render();
    }
}