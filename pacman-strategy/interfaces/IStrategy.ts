export interface IStrategy {
    render(): void;
}