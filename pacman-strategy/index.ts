import {Pacman} from "./classes/Pacman.ts";
import {TextRenderStrategy} from "./classes/TextRenderStrategy.ts";
import {HtmlRenderStrategy} from "./classes/HtmlRenderStrategy.ts";

const pacmanTextRender:Pacman = new Pacman(new TextRenderStrategy());
const pacmanHtmlRender:Pacman = new Pacman(new HtmlRenderStrategy());

pacmanTextRender.render();
pacmanHtmlRender.render();