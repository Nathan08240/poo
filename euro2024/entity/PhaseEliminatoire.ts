
import {Match} from "./Match";
import type {Equipe} from "./Equipe.ts";

export class PhaseEliminatoire {
    id: number;
    matchsEliminatoires: Match[];

    constructor() {
        this.matchsEliminatoires = [];
    }

    ajouterMatch(match: Match) {
        this.matchsEliminatoires.push(match);
    }

    simulerPhase(classementFinal: Equipe[]): void {
        console.log("Simulation de la phase éliminatoire en cours...")
        if (classementFinal.length < 16) {
            console.error("Le classement final ne contient pas suffisamment d'équipes pour simuler la phase éliminatoire.");
            return;
        }

        const equipesQualifiees = classementFinal.slice(0, 16);

        const matchsHuitiemes = this.genererMatchs(equipesQualifiees);
        this.afficherResultats("Huitièmes de finale", equipesQualifiees);

        const equipesQualifieesQuarts = this.simulerMatchsPhase(matchsHuitiemes);

        const matchsQuarts = this.genererMatchs(equipesQualifieesQuarts);
        this.afficherResultats("Quarts de finale", equipesQualifieesQuarts);

        const equipesQualifieesDemis = this.simulerMatchsPhase(matchsQuarts);

        const matchsDemis = this.genererMatchs(equipesQualifieesDemis);
        this.afficherResultats("Demi-finales", equipesQualifieesDemis);

        const equipesFinalistes = this.simulerMatchsPhase(matchsDemis);

        const matchFinale = this.genererMatchsFinale(equipesFinalistes);
        this.afficherResultats("Finale", equipesFinalistes);

        const vainqueur = this.simulerMatchFinale(matchFinale);

        console.log("Le vainqueur de la phase éliminatoire est :", vainqueur.nom);
    }

    private genererMatchs(equipes: Equipe[]): [Equipe, Equipe][] {
        const matchs: [Equipe, Equipe][] = [];
        for (let i = 0; i < equipes.length; i += 2) {
            const equipe1 = equipes[i];
            const equipe2 = equipes[i + 1];
            matchs.push([equipe1, equipe2]);
        }
        return matchs;
    }

    private simulerMatchsPhase(matchs: [Equipe, Equipe][]): Equipe[] {
        const equipesQualifiees: Equipe[] = [];
        for (const [equipe1, equipe2] of matchs) {
            const match = new Match(equipe1, equipe2);
            const vainqueur = match.simulerResultat();
            equipesQualifiees.push(vainqueur);
        }
        return equipesQualifiees;
    }

    private genererMatchsFinale(equipes: Equipe[]): [Equipe, Equipe] {
        return [equipes[0], equipes[1]];
    }

    private simulerMatchFinale(match: [Equipe, Equipe]) {
        const matchFinal = new Match(match[0], match[1]);
        return matchFinal.simulerResultat();
    }

    afficherResultats(etape: string, equipes: Equipe[]): void {
        console.log(`Résultats de la phase ${etape}:`);
        for (let i = 0; i < equipes.length; i += 2) {
            const equipe1 = equipes[i];
            const equipe2 = equipes[i + 1];
            console.log(`${equipe1.nom} vs ${equipe2.nom}`);
        }
    }
}