import {Equipe} from "./Equipe";
import type {Match} from "./Match.ts";

export class Groupe {
    id: number;
    equipes: Equipe[];
    nom: string;
    matches: Match[];

    constructor(nom: string) {
        this.equipes = [];
        this.nom = nom;
        this.equipes = [];
        this.matches = [];

    }

    ajouterEquipe(equipe: Equipe) {
        this.equipes.push(equipe);
    }
}