
import {Groupe} from "./Groupe";
import type {Equipe} from "./Equipe.ts";
import {Match} from "./Match.ts";
import {shuffle} from "../utils/methods";

export class PhaseDeGroupe {
    id: number;
    groupes: Groupe[];
    equipes: Equipe[];

    constructor() {
        this.groupes = [];
        this.equipes = [];
    }

    ajouterGroupe(groupe: Groupe) {
        this.groupes.push(groupe);
    }

    ajouterEquipe(equipe: Equipe) {
        this.equipes.push(equipe);
    }

    genererCalendrier() {
        this.tirageAuSort();
        this.simulerMatchs();
    }

    genererGroupesAleatoires() {
        const equipesMelangees = shuffle(this.equipes);

        const nbEquipesParGroupe = Math.ceil(equipesMelangees.length / this.groupes.length);

        for (let i = 0; i < this.groupes.length; i++) {
            const groupe = this.groupes[i];
            const debut = i * nbEquipesParGroupe;
            const fin = Math.min((i + 1) * nbEquipesParGroupe, equipesMelangees.length);
            groupe.equipes = equipesMelangees.slice(debut, fin);
        }
    }

    tirageAuSort() {
        this.equipes = shuffle(this.equipes);

        const nbGroupes = this.groupes.length;
        const nbEquipesParGroupe = Math.ceil(this.equipes.length / nbGroupes);
        let indexEquipe = 0;

        for (let i = 0; i < nbGroupes; i++) {
            const groupe = this.groupes[i];
            groupe.equipes = this.equipes.slice(indexEquipe, indexEquipe + nbEquipesParGroupe);
            indexEquipe += nbEquipesParGroupe;
        }
    }

    simulerMatchs() {
        for (const groupe of this.groupes) {
            for (let i = 0; i < groupe.equipes.length; i++) {
                for (let j = i + 1; j < groupe.equipes.length; j++) {
                    const match = new Match(groupe.equipes[i], groupe.equipes[j]);
                    match.simulerResultat();
                    groupe.matches.push(match);
                }
            }
        }
    }

    afficherResultats() {
        for (const groupe of this.groupes) {
            console.log(`Groupe ${groupe.nom}:`);
            for (const match of groupe.matches) {
                console.log(`${match.equipe1.nom} ${match.resultat} ${match.equipe2.nom}`);
            }
        }
    }

    calculerClassement() {
        for (const groupe of this.groupes) {
            groupe.equipes.sort((a, b) => {
                const pointsDiff = b.points - a.points;
                if (pointsDiff !== 0) {
                    return pointsDiff;
                } else {
                    return b.diff - a.diff;
                }
            });

            console.log(`Classement du Groupe ${groupe.nom}:`);
            for (let i = 0; i < groupe.equipes.length; i++) {
                const equipe = groupe.equipes[i];
                console.log(`${i + 1}. ${equipe.nom} - ${equipe.points} points - Différence de buts: ${equipe.diff}`);
            }
        }
    }

    classementFinal(): Equipe[] {
        const toutesLesEquipes: Equipe[] = this.groupes.reduce((equipes, groupe) => equipes.concat(groupe.equipes), []);

        toutesLesEquipes.sort((a, b) => {
            const pointsDiff = b.points - a.points;
            if (pointsDiff !== 0) {
                return pointsDiff;
            } else {
                return b.diff - a.diff;
            }
        });

        return toutesLesEquipes;
    }

}