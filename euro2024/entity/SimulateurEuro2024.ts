
import {PhaseDeGroupe} from "./PhaseDeGroupe";
import {PhaseEliminatoire} from "./PhaseEliminatoire";

export class SimulateurEuro2024 {
    id: number;
    phaseDeGroupe: PhaseDeGroupe;
    phaseEliminatoire: PhaseEliminatoire;
}