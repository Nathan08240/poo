

import {Equipe} from "./Equipe";

export class Match {
    id: number;
    equipe1: Equipe;
    equipe2: Equipe;
    resultat: string;

    constructor(equipe1: Equipe, equipe2: Equipe) {
        this.equipe1 = equipe1;
        this.equipe2 = equipe2;
        this.resultat = '';
    }

    simulerResultat(): Equipe {
        const random = Math.random();
        return random < 0.5 ? this.equipe1 : this.equipe2;
    }
}