export class Equipe {
    id: number;
    primary_color: string;
    secondary_color: string;
    code: string;
    hat: string;
    playoff: boolean;
    nom: string;
    points: number;
    classementFIFA: number;
    diff: number;

    constructor() {
        this.points = 0;
        this.diff = 0;
    }
}


