import {PhaseDeGroupe} from './entity/PhaseDeGroupe';
import {PhaseEliminatoire} from './entity/PhaseEliminatoire';
import {Equipe} from './entity/Equipe';
import {Groupe} from "./entity/Groupe.ts";
import equipes_data from './data/equipes.json';

const equipes: Equipe[] = equipes_data.map(data => {
    const equipe = new Equipe();
    equipe.nom = data.nom;
    equipe.primary_color = data.primary_color;
    equipe.secondary_color = data.secondary_color;
    equipe.classementFIFA = parseInt(data.classementFIFA);
    equipe.code = data.code;
    equipe.hat = data.hat;
    equipe.playoff = data.playoff;
    equipe.diff = parseInt(data.diff);
    equipe.points = parseInt(data.points);
    return equipe;
});

console.log(equipes);


const phaseDeGroupe = new PhaseDeGroupe();

equipes.forEach(equipe => phaseDeGroupe.ajouterEquipe(equipe));

const groupeA = new Groupe('A');
const groupeB = new Groupe('B');
const groupeC = new Groupe('C');
const groupeD = new Groupe('D');

phaseDeGroupe.ajouterGroupe(groupeA);
phaseDeGroupe.ajouterGroupe(groupeB);
phaseDeGroupe.ajouterGroupe(groupeC);
phaseDeGroupe.ajouterGroupe(groupeD);

phaseDeGroupe.genererGroupesAleatoires();

const phaseEliminatoire = new PhaseEliminatoire();

phaseDeGroupe.tirageAuSort();

phaseDeGroupe.genererCalendrier();

phaseDeGroupe.simulerMatchs();

phaseDeGroupe.afficherResultats();

phaseDeGroupe.calculerClassement();

phaseEliminatoire.simulerPhase(phaseDeGroupe.classementFinal());
