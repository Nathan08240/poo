# Nathan Brancos - POO Projects

Ce monorepo contient plusieurs petits projets

## Installation

Pour installer les dépendances de tous les projets, vous pouvez exécuter la commande suivante à partir du dossier parent :

```bash
npm install:all
```

ou 

```bash
yarn install:all
```

## Projets

- [x] [**`pacman-chain-of-responsibility`**](./pacman-chain-of-responsibility) - Design Patter Builder
- [x] [**`pizza`**](./pizza) - Design Patter Builder
- [x] [**`pacman-strategy`**](./pacman-strategy) - Design Patter Strategy
- [x] [**`pacman-observer`**](./pacman-observer) - Design Pattern Observer
- [x] [**`euro-2024`**](./euro2024) - Gros TP non fini

