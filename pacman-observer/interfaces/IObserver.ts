import {Direction} from "../enums/Direction.ts";
import {PacmanState} from "../enums/PacmanState.ts";

export interface IObserver {
    update(direction: Direction, state: PacmanState): void;
}