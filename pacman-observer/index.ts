import {Pacman} from "./classes/Pacman.ts";
import {Ghost} from "./classes/Ghost.ts";
import {Direction} from "./enums/Direction.ts";
import {PacmanState} from "./enums/PacmanState.ts";

const pacman = new Pacman();
const shadow = new Ghost('Shadow');
const speedy = new Ghost('Speedy');

pacman.addObserver(shadow);
pacman.addObserver(speedy);

pacman.moveTo(Direction.North);
pacman.setState(PacmanState.Super);
pacman.moveTo(Direction.East);
pacman.setState(PacmanState.Normal);
pacman.moveTo(Direction.South);