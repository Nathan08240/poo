import {Subject} from "./Subject.ts";
import {Direction} from "../enums/Direction.ts";
import {PacmanState} from "../enums/PacmanState.ts";

export class Pacman extends Subject {
    private state: PacmanState;

    constructor() {
        super();
        this.state = PacmanState.Normal;
    }

    setState(state: PacmanState) {
        console.log(`Pacman is now ${state}`);
        this.state = state;
    }

    moveTo(direction: Direction) {
        console.log(`Pacman moves to ${direction}`);
        this.notifyObservers(direction, this.state);
    }
}