import {IObserver} from "../interfaces/IObserver.ts";
import {Direction} from "../enums/Direction.ts";
import {PacmanState} from "../enums/PacmanState.ts";

export class Ghost implements IObserver {
    private name: string;

    constructor(name: string) {
        this.name = name;
    }

    update(direction: Direction, state: PacmanState) {

        if (PacmanState.Normal === state) {
            console.log(`${this.name} is moving to ${direction}`);
            return;
        }

        switch (direction) {
            case Direction.North:
                console.log(`${this.name} is moving to ${Direction.South}`);
                break;
            case Direction.South:
                console.log(`${this.name} is moving to ${Direction.North}`);
                break;
            case Direction.East:
                console.log(`${this.name} is moving to ${Direction.West}`);
                break;
            case Direction.West:
                console.log(`${this.name} is moving to ${Direction.East}`);
                break;
            default:
                console.log(`${this.name} is moving to ${direction}`);
        }
    }

}