import {IObserver} from '../interfaces/IObserver.ts';
import {Direction} from '../enums/Direction';
import {PacmanState} from '../enums/PacmanState';

export class Subject {
    private observers: IObserver[] = [];

    addObserver(observer: IObserver) {
        this.observers.push(observer);
    }

    notifyObservers(direction: Direction, state: PacmanState) {
        this.observers.forEach(observer => observer.update(direction, state));
    }
}