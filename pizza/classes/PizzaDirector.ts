import { PizzaBuilder } from './pizzaBuilder';

class PizzaDirector {
    makePizza(builder: PizzaBuilder) {
        builder.reset();
        builder.addCheese();
        builder.addMushrooms();
        builder.addPepperoni();
        builder.setDough('fine');
    }

    makeTartiflettePizza(builder: PizzaBuilder) {
        builder.reset();
        builder.addCheese();
        builder.addPotato();
        builder.addBacon();
        builder.setDough('pan');
    }
}

export default PizzaDirector;