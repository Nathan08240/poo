import {Pizza} from "./Pizza.ts";
import {IPizzaBuilder} from "../interfaces/Builder";

export class PizzaBuilder implements IPizzaBuilder {
    private pizza: Pizza;

    constructor() {
        this.reset();
    }

    reset() {
        this.pizza = new Pizza();
    }

    addCheese() {
        this.pizza.addTopping('fromage');
    }

    addPepperoni() {
        this.pizza.addTopping('pepperoni');
    }

    addMushrooms() {
        this.pizza.addTopping('jambon');
    }

    addPeppers() {
        this.pizza.addTopping('poivrons');
    }

    addSausage() {
        this.pizza.addTopping('saucisse');
    }

    addPotato() {
        this.pizza.addTopping('pomme de terre');
    }

    addBacon() {
        this.pizza.addTopping('lardons');
    }

    setDough(dough: string) {
        this.pizza.setDough(dough);
    }

    buildPizza() {
        const pizza = this.pizza;
        this.reset();
        return pizza;
    }
}