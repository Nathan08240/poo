export class Pizza {
    private toppings: string[] = [];
    private dough: string = '';

    public addTopping(topping: string) {
        this.toppings.push(topping);
    }

    public setDough(dough: string) {
        this.dough = dough;
    }

    public describe(): string {
        return `La pizza est faite avec ${this.toppings.join(', ')} et la pâte est ${this.dough}`;
    }
}
