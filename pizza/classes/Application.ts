import PizzaDirector from './PizzaDirector';
import { PizzaBuilder } from './PizzaBuilder';

class Application {
    makePizza() {
        const director = new PizzaDirector();
        const builder = new PizzaBuilder();

        director.makePizza(builder);
        const pizza = builder.buildPizza();
        console.log(pizza.describe());

        director.makeTartiflettePizza(builder);
        const pizza2 = builder.buildPizza();
        console.log(pizza2.describe());
    }
}

export default Application;
