import {Pizza} from "../classes/Pizza.ts";

export interface IPizzaBuilder {
    reset(): void;
    addCheese(): void;
    addPepperoni(): void;
    addMushrooms(): void;
    addPeppers(): void;
    addSausage(): void;
    addPotato(): void;
    addBacon(): void;
    setDough(dough: string): void;
    buildPizza(): Pizza;
}