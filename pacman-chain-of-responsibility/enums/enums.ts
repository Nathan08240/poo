import type {Dot} from "../classes/Dot.ts";
import type {Pacman} from "../classes/Pacman.ts";
import type {Fruit} from "../classes/Fruit.ts";
import {Ghost} from "../classes/Ghost.ts";


export type Entity = Ghost | Pacman | Dot | Fruit
