import type {Cell} from "../classes/Cell.ts";

export interface ICellHandler {
    setNext(handler: ICellHandler): ICellHandler;
    handle(cell: Cell): void;
}