export class Ghost {
    constructor(public name: string) {}

    static create(): Ghost {
        return new Ghost("Ghost");
    }
}