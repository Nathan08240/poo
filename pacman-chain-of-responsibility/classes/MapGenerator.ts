import {ICellHandler} from "../interfaces/ICellHandler.ts";
import {FruitHandler} from "./Handler/FruitHandler.ts";
import {DotHandler} from "./Handler/DotHandler.ts";
import {GhostHandler} from "./Handler/GhostHandler.ts";
import {PacmanHandler} from "./Handler/PacmanHandler.ts";
import {CellHandler} from "./Handler/CellHandler.ts";
import {Map} from "./Map.ts";

export class MapGenerator {
    private map: Map;
    private cellHandlers: ICellHandler = new CellHandler();

    constructor(map: Map) {
        this.map = map;
        this.buildChain();
    }

    private buildChain(): void {
        const fruitHandler = new FruitHandler();
        const dotHandler = new DotHandler();
        const ghostHandler = new GhostHandler();
        const pacmanHandler = new PacmanHandler();
        console.log(CellHandler)
        this.cellHandlers.setNext(dotHandler).setNext(ghostHandler).setNext(pacmanHandler).setNext(fruitHandler);
    }

    generateMap(): void {
        // Generate map and handle cells
        for (let cell of this.map.cells) {
            for(let y of cell) {
                this.cellHandlers.handle(y);
            }
        }
    }
}
