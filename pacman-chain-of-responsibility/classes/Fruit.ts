export class Fruit {
    constructor(public color: string) {}

    static create(): Fruit {
        return new Fruit("Yellow");
    }
}