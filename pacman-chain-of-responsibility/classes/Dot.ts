export class Dot {
    constructor(public color: string) {}

    static create(): Dot {
        return new Dot("White");
    }
}