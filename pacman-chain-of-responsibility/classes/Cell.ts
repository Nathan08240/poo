import {Entity} from "../enums/enums.ts";

export class Cell {
    private _isOccupied: boolean = false;
    private _item: Entity

    constructor(public x: number, public y: number) {}

    get isOccupied(): boolean {
        return this._isOccupied;
    }

    get item(): Entity {
        return this._item;
    }

    place(item: Entity ): void {
        this._isOccupied = true;
        this._item = item as Entity
        console.log(`Placed ${item.constructor.name} at (${this.x}, ${this.y})`);
    }

}