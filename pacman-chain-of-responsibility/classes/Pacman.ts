export class Pacman {
    constructor(public name: string) {}

    static create(): Pacman {
        return new Pacman("Pacman");
    }
}