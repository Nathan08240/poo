import { Cell } from "./Cell.ts"; // Assurez-vous d'importer correctement la classe Cell

export class Map {
    private readonly _cells: Cell[][];

    constructor(rows: number, columns: number) {
        this._cells = this.generateEmptyMap(rows, columns);
    }

    private generateEmptyMap(rows: number, columns: number): Cell[][] {
        const map: Cell[][] = [];

        for (let i = 0; i < rows; i++) {
            const row: Cell[] = [];
            for (let j = 0; j < columns; j++) {
                row.push(new Cell(i, j));
            }
            map.push(row);
        }

        return map;
    }

    get cells(): Cell[][] {
        return this._cells;
    }
}