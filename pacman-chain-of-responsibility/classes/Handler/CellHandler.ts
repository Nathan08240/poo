import {ICellHandler} from "../../interfaces/ICellHandler.ts";
import type {Cell} from "../Cell.ts";

export class CellHandler implements ICellHandler {
    private nextHandler: ICellHandler;

    setNext(handler: ICellHandler): ICellHandler {
        this.nextHandler = handler;
        return handler;
    }

    handle(cell: Cell): void {
 
    }
}