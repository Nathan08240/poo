import {ICellHandler} from "../../interfaces/ICellHandler.ts";
import {Cell} from "../Cell.ts";

export class PacmanHandler implements ICellHandler{
    private nextHandler: ICellHandler;

    setNext(handler: ICellHandler): ICellHandler {
        this.nextHandler = handler;
        return handler;
    }

    handle(cell: Cell): void {
        if (cell.type === "pacman") {
            // Handle pacman
        } else {
            this.nextHandler.handle(cell);
        }
    }
}