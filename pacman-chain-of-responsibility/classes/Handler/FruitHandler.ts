import {ICellHandler} from "../../interfaces/ICellHandler.ts";
import {Fruit} from "../Fruit.ts";
import {Cell} from "../Cell.ts";

export class FruitHandler implements ICellHandler {
    private nextHandler: ICellHandler;

    setNext(handler: ICellHandler): ICellHandler {
        this.nextHandler = handler;
        return handler;
    }

    handle(cell: Cell): void {
        if (!cell.isOccupied) {
            cell.place(Fruit.create());
        } else if (this.nextHandler) {
            this.nextHandler.handle(cell);
        }
    }
}
