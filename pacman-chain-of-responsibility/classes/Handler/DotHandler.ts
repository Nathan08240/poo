import {ICellHandler} from "../../interfaces/ICellHandler.ts";
import {Cell} from "../Cell.ts";
import {Dot} from "../Dot.ts";

export class DotHandler implements ICellHandler {
    private nextHandler: ICellHandler;

    setNext(handler: ICellHandler): ICellHandler {
        this.nextHandler = handler;
        return handler;
    }

    handle(cell: Cell): void {
        if (!cell.isOccupied) {
            cell.place(Dot.create());
        } else if (this.nextHandler) {
            this.nextHandler.handle(cell);
        }
    }
}