import type {ICellHandler} from "../../interfaces/ICellHandler.ts";

export class GhostHandler implements ICellHandler{
    private nextHandler: ICellHandler;

    setNext(handler: ICellHandler): ICellHandler {
        this.nextHandler = handler;
        return handler;
    }

    handle(cell: Cell): void {
        if (cell instanceof Ghost) {
            console.log("Handling Ghost");
        } else {
            this.nextHandler.handle(cell);
        }
    }
}