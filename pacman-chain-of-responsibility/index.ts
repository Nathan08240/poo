import {MapGenerator} from "./classes/MapGenerator.ts";
import {Map} from "./classes/Map.ts";

const map = new Map(10, 10);
const mapGenerator = new MapGenerator(map);
mapGenerator.generateMap();